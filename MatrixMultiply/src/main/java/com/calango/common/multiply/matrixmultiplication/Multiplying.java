package com.calango.common.multiply.matrixmultiplication;
import com.calango.common.multiply.MatrixBeans.MatrixBean;
import com.calango.common.multiply.MatrixBeans.MatrixResult;

/**
 *
 * @author Calango
 */
public class Multiplying {
    public void multyply(int row, int column, MatrixBean matrixBean, MatrixResult mxRes){
        int i;
        for (i = 0; i < matrixBean.getColumnLen1(); i++){
            mxRes.setMatrixResult(row, column,
                matrixBean.getMatrix1().get(row)[i] * 
                    matrixBean.getMatrix2().get(i)[column]);
        }
        //System.out.println(result);
    }
}
