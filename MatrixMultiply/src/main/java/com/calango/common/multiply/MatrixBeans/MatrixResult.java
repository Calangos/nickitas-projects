package com.calango.common.multiply.MatrixBeans;

/**
 *
 * @author Calango
 */
public class MatrixResult {
    private int[][] matrixResult;
    private int row;
    private int column;
    
    public void createMatrixResult(int row, int column){
        matrixResult = new int[row][column];
        this.row = row;
        this.column = column;
    }
    
    public int[][] getMatrixResult() {
        return matrixResult;
    }

    public void setMatrixResult(int i, int j, int value) {
        this.matrixResult[i][j] += value;
    }

    public int getRow() {
        return row;
    }

    public int getColumn() {
        return column;
    }
}
