package com.calango.common.multiply.matrixmultiplication;

import com.calango.common.multiply.MatrixBeans.MatrixBean;
import com.calango.common.multiply.MatrixBeans.MatrixResult;

/**
 *
 * @author Calango
 */
public class MultiplicationImplementation {
    public final int nThreads = 5;
    public MatrixResult multiply(MatrixBean matrixBean){
        MatrixResult result = new MatrixResult();
        WorkQueue workQueue = new WorkQueue(nThreads);
        int row,column;
        result.createMatrixResult(matrixBean.getRowLen1(),matrixBean.getColumnLen2());
        for (row = 0; row < matrixBean.getRowLen1(); row++)
            for (column = 0; column < matrixBean.getColumnLen2(); column++){
                ThreadMultiply multiplyMission = new ThreadMultiply();
                multiplyMission.setWork(row, column, matrixBean, result);
                workQueue.execute(multiplyMission); 
            }
        while (!workQueue.isAllWorkDone()) {
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        return result;
    }    
}
