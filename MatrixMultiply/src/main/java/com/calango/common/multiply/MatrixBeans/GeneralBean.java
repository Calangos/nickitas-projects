package com.calango.common.multiply.MatrixBeans;

/**
 *
 * @author Calango
 */
public class GeneralBean {
    private String userRole = "Admin";
    private String matrixName;

    public String getUserRole() {
        return userRole;
    }

    public void setUserRole(String userRole) {
        this.userRole = userRole;
    }

    public String getMatrixName() {
        return matrixName;
    }

    public void setMatrixName(String matrixName) {
        this.matrixName = matrixName;
    }
}
