package com.calango.common.multiply.matrixmultiplication;

import com.calango.common.multiply.MatrixBeans.MatrixBean;
import com.calango.common.multiply.MatrixBeans.MatrixResult;

/**
 *
 * @author Calango
 */
public class ThreadMultiply implements Runnable{
    private int rowID;
    private int columnID;
    private MatrixBean matrixBean;
    private MatrixResult mxRes;
    
    public void setWork(int row, int column, MatrixBean matrixBean, MatrixResult mxRes){
        rowID = row;
        columnID = column;
        this.matrixBean = matrixBean;
        this.mxRes = mxRes;
    }
        
    @Override
        public synchronized void run(){ 
            Multiplying multiplying = new Multiplying();
            multiplying.multyply(rowID, columnID, matrixBean, mxRes);
        }
}
