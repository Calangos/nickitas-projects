package com.calango.daolayer.model;

import java.io.Serializable;
import java.sql.Timestamp;
import javax.persistence.*;

/**
 *
 * @author Calango
 */
@Entity
@Table(name="history")
public class History implements Serializable{
    private static final long serialVersionUID = 1L;
    @Id
    @Column(name = "history_id", nullable = false)        
    int historyId;
    @Column(name = "matrix1_id")
    int matrix1Id;
    @Column(name = "matrix2_id")
    int matrix2Id;
    @Column(name = "matrix_result_id")
    int matrixResultId;
    @Column(name = "date")
    @Temporal(TemporalType.TIMESTAMP)        
    Timestamp date;

    public History(){}
    
    public History
        (int historyId, int matrix1ID, int matrix2ID, 
                int matrixResultID, Timestamp date) {
        this.historyId = historyId;
        this.matrix1Id = matrix1ID;
        this.matrix2Id = matrix2ID;
        this.matrixResultId = matrixResultID;
        this.date = date;
    }

    public int getHistoryId() {
        return historyId;
    }    
        
    public int getMatrix1Id() {
        return matrix1Id;
    }

    public int getMatrix2Id() {
        return matrix2Id;
    }

    public int getMatrixResultId() {
        return matrixResultId;
    }

    public Timestamp getDate() {
        return date;
    }

    public void setHistoryId(int historyId) {
        this.historyId = historyId;
    }

    public void setMatrix1Id(int matrix1Id) {
        this.matrix1Id = matrix1Id;
    }

    public void setMatrix2Id(int matrix2Id) {
        this.matrix2Id = matrix2Id;
    }

    public void setMatrixResultId(int matrixResultId) {
        this.matrixResultId = matrixResultId;
    }

    public void setDate(Timestamp date) {
        this.date = date;
    }
}
